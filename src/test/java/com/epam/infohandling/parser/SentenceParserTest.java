package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SentenceParserTest {
    private static final String TEXT = "The example sentence: 15 12 - end.";

    private final Parser sentenceParser = new SentenceParser();

    @Test
    public void shouldParseSentence() {
        //given
        //when
        Component component = sentenceParser.parse(TEXT);
        //then
        List<Component> words = component.getChildren();
        int wordsAmount = words.size();
        Assert.assertEquals(5, wordsAmount);

        Component first = words.get(0);
        Assert.assertEquals(Lexeme.word("The"), first);

        Component second = words.get(1);
        Assert.assertEquals(Lexeme.word("example"), second);

        Component third = words.get(2);
        Assert.assertEquals(Lexeme.word("sentence:"), third);

        Component fourth = words.get(3);
        Assert.assertEquals(Lexeme.expression("15 12 -"), fourth);

        Component fifth = words.get(4);
        Assert.assertEquals(Lexeme.word("end."), fifth);
    }

}
