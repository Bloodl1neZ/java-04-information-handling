package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;


public class TextParserTest {
    private static final String FIRST_PARAGRAPH = "This is first 45 5 * sentence... This is second 6 25 / sentence!";
    private static final String SECOND_PARAGRAPH = "This is second 6 25 / sentence! This is first 45 5 * sentence...";
    private static final String THIRD_PARAGRAPH = "This is second 6 25 / sentence! This is first 45 5 * sentence... " +
            "This is third 45 5 * sentence!";

    private static final String PARAGRAPH_SPLITTER = "\n";
    private static final String TEXT = FIRST_PARAGRAPH + PARAGRAPH_SPLITTER + SECOND_PARAGRAPH +
            PARAGRAPH_SPLITTER + THIRD_PARAGRAPH;

    @Test
    public void shouldParse() {
        //given
        Parser mock = Mockito.mock(Parser.class);
        Mockito.when(mock.parse(Mockito.anyString())).thenAnswer(new Answer<Component>() {
            public Component answer(InvocationOnMock invocationOnMock) {
                Object[] args = invocationOnMock.getArguments();
                String text = (String)args[0];
                return Lexeme.word(text);
            }
        });
        Parser textParser = new TextParser(mock);
        //when
        Component paragraphsComp = textParser.parse(TEXT);
        //then
        List<Component> sentences = paragraphsComp.getChildren();
        Assert.assertEquals(3, sentences.size());

        Component first = sentences.get(0);
        Assert.assertEquals(Lexeme.word(FIRST_PARAGRAPH), first);

        Component second = sentences.get(1);
        Assert.assertEquals(Lexeme.word(SECOND_PARAGRAPH), second);

        Component third = sentences.get(2);
        Assert.assertEquals(Lexeme.word(THIRD_PARAGRAPH), third);
    }
}
