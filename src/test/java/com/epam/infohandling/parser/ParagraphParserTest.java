package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

public class ParagraphParserTest {
    private final static String FIRST_SENTENCE = "This is first 45 5 * sentence...";
    private final static String SECOND_SENTENCE = "This is second 6 25 / sentence?";
    private final static String PARAGRAPH = FIRST_SENTENCE + SECOND_SENTENCE;

    @Test
    public void shouldParseParagraph() {
        //given
        Parser mock = Mockito.mock(Parser.class);
        Mockito.when(mock.parse(Mockito.anyString())).thenAnswer(new Answer<Component>() {
            public Component answer(InvocationOnMock invocationOnMock) {
               Object[] args = invocationOnMock.getArguments();
               String text = (String)args[0];
               return Lexeme.word(text);
            }
        });
        Parser paragraphParser = new ParagraphParser(mock);
        //when
        Component sentencesComp = paragraphParser.parse(PARAGRAPH);
        //then
        List<Component> sentences = sentencesComp.getChildren();
        Assert.assertEquals(2, sentences.size());

        Component first = sentences.get(0);
        Assert.assertEquals(Lexeme.word(FIRST_SENTENCE), first);

        Component second = sentences.get(1);
        Assert.assertEquals(Lexeme.word(SECOND_SENTENCE), second);
    }
}