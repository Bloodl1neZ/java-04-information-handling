package com.epam.infohandling.parser;

import org.junit.Assert;
import org.junit.Test;


public class ChainBuilderTest {
    private final ChainBuilder chainBuilder = new ChainBuilder();

    @Test
    public void shouldCreateChain() {
        //given
        //when
        Parser textParser = chainBuilder.buildChain();
        //then
        Assert.assertEquals(TextParser.class, textParser.getClass());

        Parser paragraphParser = textParser.getSuccessor();
        Assert.assertEquals(ParagraphParser.class, paragraphParser.getClass());

        Parser sentenceParser = paragraphParser.getSuccessor();
        Assert.assertEquals(SentenceParser.class, sentenceParser.getClass());
    }
}
