package com.epam.infohandling.cloner;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import com.epam.infohandling.model.Lexeme;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TextClonerTest {
    private static final Component FIRST_LEXEME = Lexeme.word("Hi.");
    private static final Component SECOND_LEXEME = Lexeme.expression("It's");
    private static final Component THIRD_LEXEME = Lexeme.expression("5 4 -");

    private static final Component FIRST_SENTENCE = new Composite();
    private static final Component SECOND_SENTENCE = new Composite();

    private static final Component FIRST_PARAGRAPH = new Composite();
    private static final Component SECOND_PARAGRAPH = new Composite();

    private static final Component TEXT = new Composite();

    private final TextCloner textCloner = new TextCloner();

    {
        FIRST_SENTENCE.addComponent(FIRST_LEXEME);

        SECOND_SENTENCE.addComponent(SECOND_LEXEME);
        SECOND_SENTENCE.addComponent(THIRD_LEXEME);

        FIRST_PARAGRAPH.addComponent(FIRST_SENTENCE);

        SECOND_PARAGRAPH.addComponent(SECOND_SENTENCE);

        TEXT.addComponent(FIRST_PARAGRAPH);
        TEXT.addComponent(SECOND_PARAGRAPH);
    }

    @Test
    public void shouldClone() {
        //given
        //when
        Component result = textCloner.clone(TEXT);
        //then
        List<Component> paragraphs = result.getChildren();
        Assert.assertEquals(2, paragraphs.size());

        Component firstParagraph = paragraphs.get(0);
        List<Component> firstSentences = firstParagraph.getChildren();
        Assert.assertEquals(1, firstSentences.size());

        Component firstSentence = firstSentences.get(0);
        List<Component> firstLexemes = firstSentence.getChildren();

        Assert.assertEquals(1, firstLexemes.size());

        Component firstLexeme = firstLexemes.get(0);
        Assert.assertEquals(FIRST_LEXEME, firstLexeme);

        Component secondParagraph = paragraphs.get(1);
        List<Component> secondSentences = secondParagraph.getChildren();
        Assert.assertEquals(1, secondSentences.size());

        Component secondSentence = secondSentences.get(0);
        List<Component> secondLexemes = secondSentence.getChildren();

        Assert.assertEquals(2, secondLexemes.size());

        Component secondLexeme = secondLexemes.get(0);
        Assert.assertEquals(SECOND_LEXEME, secondLexeme);

        Component thirdLexeme = secondLexemes.get(1);
        Assert.assertEquals(THIRD_LEXEME, thirdLexeme);
    }
}
