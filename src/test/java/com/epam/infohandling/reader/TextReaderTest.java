package com.epam.infohandling.reader;

import com.epam.infohandling.exception.InvalidPathException;
import org.junit.Assert;
import org.junit.Test;

public class TextReaderTest {
    private static final String VALID_PATH = "src/test/resources/input.txt";
    private static final String INVALID_PATH = "qwesrc/test/resources/input.txt";

    private static final String TEXT = "It has survived - not only (five) centuries, but also " +
            "the leap into 3 4 + electronic typesetting. I'm 6 3 4 + * years old.\n" +
            "This is example text to check parser. Second sentence!\n" +
            "New paragraph. Sentence number 2 1 / in this paragraph.\n" +
            "Bye.\n";

    private final TextReader fileParser = new TextReader();

    @Test
    public void shouldParseFile() throws InvalidPathException {
        //given
        //when
        String result = fileParser.parse(VALID_PATH);
        //then
        Assert.assertEquals(TEXT, result);
    }

    @Test(expected = InvalidPathException.class)
    public void shouldNotParseFile() throws InvalidPathException {
        //given
        //when
        String result = fileParser.parse(INVALID_PATH);
    }
}
