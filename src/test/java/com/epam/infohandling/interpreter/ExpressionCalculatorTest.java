package com.epam.infohandling.interpreter;

import org.junit.Assert;
import org.junit.Test;

public class ExpressionCalculatorTest {
    private static final String FIRST_EXPRESSION = "8 2 7 4 + * -";
    private static final String SECOND_EXPRESSION = "8 2 +";

    private final ExpressionCalculator calculator = new ExpressionCalculator();

    @Test
    public void shouldCalculateAndReturnFourteen() {
        //given
        //when
        int value = calculator.calculate(FIRST_EXPRESSION);
        //then
        Assert.assertEquals(14, value);
    }

    @Test
    public void shouldCalculateAndReturnTen() {
        //given
        //when
        int value = calculator.calculate(SECOND_EXPRESSION);
        //then
        Assert.assertEquals(10, value);
    }
}
