package com.epam.infohandling.sorter;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Comparator;
import java.util.List;

public class ParagraphSorterTest {
    private static final Component SENTENCE = new Composite();

    private static final Component FIRST_PARAGRAPH = new Composite();
    private static final Component SECOND_PARAGRAPH = new Composite();
    private static final Component THIRD_PARAGRAPH = new Composite();

    private static final Component TEXT = new Composite();

    {
        FIRST_PARAGRAPH.addComponent(SENTENCE);
        FIRST_PARAGRAPH.addComponent(SENTENCE);
        FIRST_PARAGRAPH.addComponent(SENTENCE);

        SECOND_PARAGRAPH.addComponent(SENTENCE);

        THIRD_PARAGRAPH.addComponent(SENTENCE);
        THIRD_PARAGRAPH.addComponent(SENTENCE);

        TEXT.addComponent(FIRST_PARAGRAPH);
        TEXT.addComponent(SECOND_PARAGRAPH);
        TEXT.addComponent(THIRD_PARAGRAPH);
    }

    @Test
    public void shouldSortParagraphsByComparatorWitchReturnsOne() {
        //given
        Comparator<Component> mock = Mockito.mock(Comparator.class);
        Mockito.when(mock.compare(Mockito.any(Component.class), Mockito.any(Component.class))).thenReturn(1);

        Sorter sorter = new ParagraphSorter(mock);
        //when
        Component sortedText = sorter.sort(TEXT);
        //then
        List<Component> paragraphs = sortedText.getChildren();
        Assert.assertEquals(3, paragraphs.size());

        Component firstParagraph = paragraphs.get(0);
        List<Component> firstSentences = firstParagraph.getChildren();
        Assert.assertEquals(3, firstSentences.size());

        Component secondParagraph = paragraphs.get(1);
        List<Component> secondSentences = secondParagraph.getChildren();
        Assert.assertEquals(1, secondSentences.size());

        Component thirdParagraph = paragraphs.get(2);
        List<Component> thirdSentences = thirdParagraph.getChildren();
        Assert.assertEquals(2, thirdSentences.size());
    }

    @Test
    public void shouldSortParagraphsByComparatorWitchReturnsMinusOne() {
        //given
        Comparator<Component> mock = Mockito.mock(Comparator.class);
        Mockito.when(mock.compare(Mockito.any(Component.class), Mockito.any(Component.class))).thenReturn(-1);

        Sorter sorter = new ParagraphSorter(mock);
        //when
        Component sortedText = sorter.sort(TEXT);
        //then
        List<Component> paragraphs = sortedText.getChildren();
        Assert.assertEquals(3, paragraphs.size());

        Component firstParagraph = paragraphs.get(0);
        List<Component> firstSentences = firstParagraph.getChildren();
        Assert.assertEquals(2, firstSentences.size());

        Component secondParagraph = paragraphs.get(1);
        List<Component> secondSentences = secondParagraph.getChildren();
        Assert.assertEquals(1, secondSentences.size());

        Component thirdParagraph = paragraphs.get(2);
        List<Component> thirdSentences = thirdParagraph.getChildren();
        Assert.assertEquals(3, thirdSentences.size());
    }

    @Test
    public void shouldSortParagraphsByComparatorWitchReturnsZero() {
        //given
        Comparator<Component> mock = Mockito.mock(Comparator.class);
        Mockito.when(mock.compare(Mockito.any(Component.class), Mockito.any(Component.class))).thenReturn(0);

        Sorter sorter = new ParagraphSorter(mock);
        //when
        Component sortedText = sorter.sort(TEXT);
        //then
        List<Component> paragraphs = sortedText.getChildren();
        Assert.assertEquals(3, paragraphs.size());

        Component firstParagraph = paragraphs.get(0);
        List<Component> firstSentences = firstParagraph.getChildren();
        Assert.assertEquals(3, firstSentences.size());

        Component secondParagraph = paragraphs.get(1);
        List<Component> secondSentences = secondParagraph.getChildren();
        Assert.assertEquals(1, secondSentences.size());

        Component thirdParagraph = paragraphs.get(2);
        List<Component> thirdSentences = thirdParagraph.getChildren();
        Assert.assertEquals(2, thirdSentences.size());
    }
}
