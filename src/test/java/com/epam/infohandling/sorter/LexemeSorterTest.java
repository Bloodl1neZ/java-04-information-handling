package com.epam.infohandling.sorter;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import com.epam.infohandling.model.Lexeme;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Comparator;
import java.util.List;

public class LexemeSorterTest {
    private static final Component FIRST_LEXEME = Lexeme.word("Hi,");
    private static final Component SECOND_LEXEME = Lexeme.word("friend.");
    private static final Component THIRD_LEXEME = Lexeme.word("dear");

    private static final Component FIRST_SENTENCE = new Composite();
    private static final Component SECOND_SENTENCE = new Composite();

    private static final Component PARAGRAPH = new Composite();

    private static final Component TEXT = new Composite();

    {
        FIRST_SENTENCE.addComponent(FIRST_LEXEME);
        FIRST_SENTENCE.addComponent(SECOND_LEXEME);

        SECOND_SENTENCE.addComponent(FIRST_LEXEME);
        SECOND_SENTENCE.addComponent(THIRD_LEXEME);
        SECOND_SENTENCE.addComponent(SECOND_LEXEME);

        PARAGRAPH.addComponent(FIRST_SENTENCE);
        PARAGRAPH.addComponent(SECOND_SENTENCE);

        TEXT.addComponent(PARAGRAPH);
    }

    @Test
    public void shouldSortParagraphsByComparatorWitchReturnsOne() {
        Comparator<Component> mock = Mockito.mock(Comparator.class);
        Mockito.when(mock.compare(Mockito.any(Component.class), Mockito.any(Component.class))).thenReturn(1);

        Sorter sorter = new LexemeSorter(mock);
        //given
        //when
        Component sortedText = sorter.sort(TEXT);
        //then
        List<Component> paragraphs = sortedText.getChildren();
        Assert.assertEquals(1, paragraphs.size());

        Component paragraph = paragraphs.get(0);
        List<Component> sentences = paragraph.getChildren();
        Assert.assertEquals(2, sentences.size());

        Component firstSentence = sentences.get(0);
        List<Component> firstLexemes = firstSentence.getChildren();
        Assert.assertEquals(2, firstLexemes.size());

        Component firstLexeme = firstLexemes.get(0);
        Assert.assertEquals(FIRST_LEXEME, firstLexeme);
        Component secondLexeme = firstLexemes.get(1);
        Assert.assertEquals(SECOND_LEXEME, secondLexeme);

        Component secondSentence = sentences.get(1);
        List<Component> secondLexemes = secondSentence.getChildren();
        Assert.assertEquals(3, secondLexemes.size());

        Component thirdLexeme = secondLexemes.get(0);
        Assert.assertEquals(FIRST_LEXEME, thirdLexeme);
        Component fourthLexeme = secondLexemes.get(1);
        Assert.assertEquals(THIRD_LEXEME, fourthLexeme);
        Component fifthLexeme = secondLexemes.get(2);
        Assert.assertEquals(SECOND_LEXEME, fifthLexeme);
    }

    @Test
    public void shouldSortParagraphsByComparatorWitchReturnsMinusOne() {
        Comparator<Component> mock = Mockito.mock(Comparator.class);
        Mockito.when(mock.compare(Mockito.any(Component.class), Mockito.any(Component.class))).thenReturn(-1);

        Sorter sorter = new LexemeSorter(mock);
        //given
        //when
        Component sortedText = sorter.sort(TEXT);
        //then
        List<Component> paragraphs = sortedText.getChildren();
        Assert.assertEquals(1, paragraphs.size());

        Component paragraph = paragraphs.get(0);
        List<Component> sentences = paragraph.getChildren();
        Assert.assertEquals(2, sentences.size());

        Component firstSentence = sentences.get(0);
        List<Component> firstLexemes = firstSentence.getChildren();
        Assert.assertEquals(2, firstLexemes.size());

        Component firstLexeme = firstLexemes.get(0);
        Assert.assertEquals(SECOND_LEXEME, firstLexeme);
        Component secondLexeme = firstLexemes.get(1);
        Assert.assertEquals(FIRST_LEXEME, secondLexeme);

        Component secondSentence = sentences.get(1);
        List<Component> secondLexemes = secondSentence.getChildren();
        Assert.assertEquals(3, secondLexemes.size());

        Component thirdLexeme = secondLexemes.get(0);
        Assert.assertEquals(SECOND_LEXEME, thirdLexeme);
        Component fourthLexeme = secondLexemes.get(1);
        Assert.assertEquals(THIRD_LEXEME, fourthLexeme);
        Component fifthLexeme = secondLexemes.get(2);
        Assert.assertEquals(FIRST_LEXEME, fifthLexeme);
    }

    @Test
    public void shouldSortParagraphsByComparatorWitchReturnsZero() {
        Comparator<Component> mock = Mockito.mock(Comparator.class);
        Mockito.when(mock.compare(Mockito.any(Component.class), Mockito.any(Component.class))).thenReturn(0);

        Sorter sorter = new LexemeSorter(mock);
        //given
        //when
        Component sortedText = sorter.sort(TEXT);
        //then
        List<Component> paragraphs = sortedText.getChildren();
        Assert.assertEquals(1, paragraphs.size());

        Component paragraph = paragraphs.get(0);
        List<Component> sentences = paragraph.getChildren();
        Assert.assertEquals(2, sentences.size());

        Component firstSentence = sentences.get(0);
        List<Component> firstLexemes = firstSentence.getChildren();
        Assert.assertEquals(2, firstLexemes.size());

        Component firstLexeme = firstLexemes.get(0);
        Assert.assertEquals(FIRST_LEXEME, firstLexeme);
        Component secondLexeme = firstLexemes.get(1);
        Assert.assertEquals(SECOND_LEXEME, secondLexeme);

        Component secondSentence = sentences.get(1);
        List<Component> secondLexemes = secondSentence.getChildren();
        Assert.assertEquals(3, secondLexemes.size());

        Component thirdLexeme = secondLexemes.get(0);
        Assert.assertEquals(FIRST_LEXEME, thirdLexeme);
        Component fourthLexeme = secondLexemes.get(1);
        Assert.assertEquals(THIRD_LEXEME, fourthLexeme);
        Component fifthLexeme = secondLexemes.get(2);
        Assert.assertEquals(SECOND_LEXEME, fifthLexeme);
    }
}
