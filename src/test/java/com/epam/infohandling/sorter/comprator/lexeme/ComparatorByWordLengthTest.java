package com.epam.infohandling.sorter.comprator.lexeme;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;
import com.epam.infohandling.sorter.comparator.lexeme.ComparatorByWordLength;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class ComparatorByWordLengthTest {
    private static final Component FIRST_LEXEME = Lexeme.word("Have");
    private static final Component SECOND_LEXEME = Lexeme.word("day");
    private static final Component THIRD_LEXEME = Lexeme.word("good");

    private final Comparator<Component> comparator = new ComparatorByWordLength();

    @Test
    public void shouldCompareAndReturnOne() {
        //given
        //when
        int result = comparator.compare(FIRST_LEXEME, SECOND_LEXEME);
        //then
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldCompareAndReturnMinusOne() {
        //given
        //when
        int result = comparator.compare(SECOND_LEXEME, THIRD_LEXEME);
        //then
        Assert.assertEquals(-1, result);
    }

    @Test
    public void shouldCompareAndReturnZero() {
        //given
        //when
        int result = comparator.compare(FIRST_LEXEME, THIRD_LEXEME);
        //then
        Assert.assertEquals(0, result);
    }

}
