package com.epam.infohandling.sorter.comprator.lexeme;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;
import com.epam.infohandling.sorter.comparator.lexeme.ComparatorByCharMatches;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class ComparatorByCharMatchesTest {
    private static final Component FIRST_LEXEME = Lexeme.word("apple");
    private static final Component SECOND_LEXEME = Lexeme.word("hollow");
    private static final Component THIRD_LEXEME = Lexeme.word("lemon");

    private static final char CRITERION_SYMBOL = 'l';

    private final Comparator<Component> comparator = new ComparatorByCharMatches(CRITERION_SYMBOL);

    @Test
    public void shouldCompareLexemesAndReturnOne() {
        //given
        //when
        int result = comparator.compare(FIRST_LEXEME, SECOND_LEXEME);
        //then
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldCompareSameLexemesAndReturnMinusOne() {
        //given
        //when
        int result = comparator.compare(SECOND_LEXEME, FIRST_LEXEME);
        //then
        Assert.assertEquals(-1, result);
    }

    @Test
    public void shouldCompareLexemesAndReturnZero() {
        //given
        //when
        int result = comparator.compare(FIRST_LEXEME, THIRD_LEXEME);
        //then
        Assert.assertEquals(0, result);
    }
}
