package com.epam.infohandling.sorter.comprator.paragraph;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import com.epam.infohandling.model.Lexeme;
import com.epam.infohandling.sorter.comparator.paragraph.ComparatorBySentenceAmount;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class ComparatorBySentenceAmountTest {
    private static final Component FIRST_LEXEME = Lexeme.word("Hi,");
    private static final Component SECOND_LEXEME = Lexeme.word("mate.");
    private static final Component THIRD_LEXEME = Lexeme.word("Hello,");
    private static final Component FOURTH_LEXEME = Lexeme.word("boss.");

    private static final Component FIRST_SENTENCE = new Composite();
    private static final Component SECOND_SENTENCE = new Composite();

    private static final Component FIRST_PARAGRAPH = new Composite();
    private static final Component SECOND_PARAGRAPH = new Composite();
    private static final Component THIRD_PARAGRAPH = new Composite();

    private final Comparator<Component> comparator = new ComparatorBySentenceAmount();

    {
        FIRST_SENTENCE.addComponent(FIRST_LEXEME);
        FIRST_SENTENCE.addComponent(SECOND_LEXEME);

        SECOND_SENTENCE.addComponent(THIRD_LEXEME);
        SECOND_SENTENCE.addComponent(FOURTH_LEXEME);

        FIRST_PARAGRAPH.addComponent(FIRST_SENTENCE);
        FIRST_PARAGRAPH.addComponent(SECOND_SENTENCE);

        SECOND_PARAGRAPH.addComponent(SECOND_SENTENCE);
        SECOND_PARAGRAPH.addComponent(FIRST_SENTENCE);
        SECOND_PARAGRAPH.addComponent(SECOND_SENTENCE);

        THIRD_PARAGRAPH.addComponent(SECOND_SENTENCE);
        THIRD_PARAGRAPH.addComponent(FIRST_SENTENCE);
    }

    @Test
    public void shouldCompareParagraphsAndReturnMinusOne() {
        //given
        //when
        int result = comparator.compare(FIRST_PARAGRAPH, SECOND_PARAGRAPH);
        //then
        Assert.assertEquals(-1, result);
    }

    @Test
    public void shouldCompareParagraphsAndReturnOne() {
        //given
        //when
        int result = comparator.compare(SECOND_PARAGRAPH, FIRST_PARAGRAPH);
        //then
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldCompareParagraphsAndReturnZero() {
        //given
        //when
        int result = comparator.compare(THIRD_PARAGRAPH, FIRST_PARAGRAPH);
        //then
        Assert.assertEquals(0, result);
    }


}
