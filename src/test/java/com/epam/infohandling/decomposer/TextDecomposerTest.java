package com.epam.infohandling.decomposer;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import com.epam.infohandling.model.Lexeme;
import org.junit.Assert;
import org.junit.Test;

public class TextDecomposerTest {
    private static final Component FIRST_LEXEME = Lexeme.word("Hi,");
    private static final Component SECOND_LEXEME = Lexeme.word("mate.");
    private static final Component THIRD_LEXEME = Lexeme.expression("51 400 +");
    private static final Component FOURTH_LEXEME = Lexeme.word("degrees.");

    private static final Component FIRST_SENTENCE = new Composite();
    private static final Component SECOND_SENTENCE = new Composite();

    private static final Component FIRST_PARAGRAPH = new Composite();
    private static final Component SECOND_PARAGRAPH = new Composite();

    private static final Component TEXT = new Composite();
    private static final String GENERATED_TEXT = "Hi, mate. 451 degrees. \n" +
            "451 degrees. Hi, mate. \n";

    {
        FIRST_SENTENCE.addComponent(FIRST_LEXEME);
        FIRST_SENTENCE.addComponent(SECOND_LEXEME);

        SECOND_SENTENCE.addComponent(THIRD_LEXEME);
        SECOND_SENTENCE.addComponent(FOURTH_LEXEME);

        FIRST_PARAGRAPH.addComponent(FIRST_SENTENCE);
        FIRST_PARAGRAPH.addComponent(SECOND_SENTENCE);

        SECOND_PARAGRAPH.addComponent(SECOND_SENTENCE);
        SECOND_PARAGRAPH.addComponent(FIRST_SENTENCE);

        TEXT.addComponent(FIRST_PARAGRAPH);
        TEXT.addComponent(SECOND_PARAGRAPH);
    }

    @Test
    public void shouldCreateString() {
        //given
        TextDecomposer textDecomposer = new TextDecomposer();
        //when
        String result = textDecomposer.createText(TEXT);
        //then
        Assert.assertEquals(GENERATED_TEXT, result);
    }

}
