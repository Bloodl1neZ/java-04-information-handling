package com.epam.infohandling.sorter;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.cloner.TextCloner;

import java.util.Comparator;
import java.util.List;

public class LexemeSorter implements Sorter {
    private final Comparator<Component> comparator;

    public LexemeSorter(final Comparator<Component> comparator) {
        this.comparator = comparator;
    }

    public Component sort(final Component text) {
        TextCloner textCloner = new TextCloner();
        Component sortedText = textCloner.clone(text);
        List<Component> paragraphs = sortedText.getChildren();
        paragraphs.forEach( paragraph -> sortParagraph(paragraph) );
        return sortedText;
    }

    private void sortParagraph(Component paragraph) {
        List<Component> sentences = paragraph.getChildren();
        sentences.forEach( sentence -> {
            List<Component> lexemes = sentence.getChildren();
            lexemes.sort(comparator);
        });
    }
}
