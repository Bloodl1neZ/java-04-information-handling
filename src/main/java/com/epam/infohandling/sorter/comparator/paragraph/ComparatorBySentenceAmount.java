package com.epam.infohandling.sorter.comparator.paragraph;

import com.epam.infohandling.model.Component;

import java.util.Comparator;
import java.util.List;

public class ComparatorBySentenceAmount implements Comparator<Component> {
    @Override
    public int compare(final Component firstParagraph, final Component secondParagraph) {
        List<Component> firstSentences = firstParagraph.getChildren();
        List<Component> secondSentences = secondParagraph.getChildren();
        int firstSentencesAmount = firstSentences.size();
        int secondSentencesAmount = secondSentences.size();
        return Integer.compare(firstSentencesAmount, secondSentencesAmount);
    }
}
