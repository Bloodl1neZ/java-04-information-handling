package com.epam.infohandling.sorter.comparator.lexeme;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;

import java.util.Comparator;

public class ComparatorByWordLength implements Comparator<Component> {
    @Override
    public int compare(final Component firstLexeme, final Component secondLexeme) {
        String firstValue = ((Lexeme)firstLexeme).getValue();
        String secondValue = ((Lexeme)secondLexeme).getValue();
        int firstValueLength = firstValue.length();
        int secondValueLength = secondValue.length();
        return Integer.compare(firstValueLength, secondValueLength);
    }
}
