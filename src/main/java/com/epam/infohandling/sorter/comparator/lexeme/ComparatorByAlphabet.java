package com.epam.infohandling.sorter.comparator.lexeme;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;

import java.util.Comparator;

public class ComparatorByAlphabet implements Comparator<Component> {
    @Override
    public int compare(final Component firstLexeme, final Component secondLexeme) {
        String firstValue = ((Lexeme)firstLexeme).getValue();
        String secondValue = ((Lexeme)secondLexeme).getValue();
        return firstValue.compareTo(secondValue);
    }
}
