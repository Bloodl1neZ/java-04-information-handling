package com.epam.infohandling.sorter.comparator.lexeme;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;

import java.util.Comparator;
import java.util.stream.IntStream;

public class ComparatorByCharMatches implements Comparator<Component> {
    private final char symbol;

    public ComparatorByCharMatches(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public int compare(final Component firstLexeme, final Component secondLexeme) {
        long firstMatches = countMatches(firstLexeme);
        long secondMatches = countMatches(secondLexeme);
        return Long.compare(secondMatches, firstMatches);
    }

    private long countMatches(final Component lexeme) {
        String value = ((Lexeme)lexeme).getValue();
        IntStream chars = value.chars();
        IntStream filtered = chars.filter(ch -> ch == symbol);
        long matchesNumber = filtered.count();
        return matchesNumber;
    }
}
