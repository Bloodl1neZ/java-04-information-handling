package com.epam.infohandling.sorter;

import com.epam.infohandling.model.Component;

public interface Sorter {
    Component sort(Component component);
}
