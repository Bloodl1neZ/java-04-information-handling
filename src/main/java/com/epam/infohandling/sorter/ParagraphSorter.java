package com.epam.infohandling.sorter;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.cloner.TextCloner;

import java.util.Comparator;
import java.util.List;

public class ParagraphSorter implements Sorter {
    private final Comparator<Component> comparator;

    public ParagraphSorter(final Comparator<Component> comparator) {
        this.comparator = comparator;
    }

    public Component sort(final Component text) {
        TextCloner textCloner = new TextCloner();
        Component sortedText = textCloner.clone(text);
        List<Component> paragraphs = sortedText.getChildren();
        paragraphs.sort(comparator);
        return sortedText;
    }
}
