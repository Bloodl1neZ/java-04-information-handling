package com.epam.infohandling.reader;

import com.epam.infohandling.exception.InvalidPathException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextReader {
    private static final Logger LOGGER = LogManager.getLogger(TextReader.class);

    public String parse(final String path) throws InvalidPathException {
        try {
            StringBuilder text = new StringBuilder();
            Scanner scanner = new Scanner(new java.io.FileReader(path));
            LOGGER.info("path is valid");
            while (scanner.hasNextLine()) {
                String paragraph = scanner.nextLine();
                text.append(paragraph);
                text.append("\n");
            }
            LOGGER.info("text has been created");
            return text.toString();
        } catch (FileNotFoundException e) {
            throw new InvalidPathException(path, e);
        }
    }
}
