package com.epam.infohandling.exception;

public class InvalidPathException extends Exception {
    public InvalidPathException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
