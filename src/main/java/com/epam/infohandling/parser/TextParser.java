package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;

public class TextParser extends Parser {
    private static final String PARAGRAPH_SPLITTER_REG_EX = "\\n";

    public TextParser(Parser parser) {
        super(parser);
    }

    public Component parse(final String text) {
        String[] parts = text.split(PARAGRAPH_SPLITTER_REG_EX);
        Component paragraphs = new Composite();
        Parser successor = getSuccessor();
        for(String paragraph : parts) {
            Component sentences = successor.parse(paragraph);
            paragraphs.addComponent(sentences);
        }
        return paragraphs;
    }
}
