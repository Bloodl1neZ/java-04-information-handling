package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser extends Parser {
    private static final String SENTENCE_REG_EXP = "[A-Z].*?((\\.{3})|\\.|!|\\?)";

    public ParagraphParser(final Parser parser) {
        super(parser);
    }

    public Component parse(final String text) {
        Pattern pattern = Pattern.compile(SENTENCE_REG_EXP);
        Matcher matcher = pattern.matcher(text);
        Component sentences = new Composite();
        Parser successor = getSuccessor();
        while(matcher.find()) {
            String sentence = matcher.group();
            Component words = successor.parse(sentence);
            sentences.addComponent(words);
        }
        return sentences;
    }
}