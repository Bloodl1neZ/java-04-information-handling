package com.epam.infohandling.parser;

public class ChainBuilder {
    public Parser buildChain() {
        Parser sentenceParser = new SentenceParser();
        Parser paragraphParser = new ParagraphParser(sentenceParser);
        Parser textParser = new TextParser(paragraphParser);
        return textParser;
    }
}
