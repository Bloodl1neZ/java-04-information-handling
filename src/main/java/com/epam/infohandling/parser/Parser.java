package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;

public abstract class Parser {
    private Parser successor;

    public Parser(final Parser successor) {
        this.successor = successor;
    }

    public Parser() {

    }

    public Parser getSuccessor() {
        return successor;
    }

    public abstract Component parse(final String text);
}
