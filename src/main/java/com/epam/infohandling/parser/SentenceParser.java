package com.epam.infohandling.parser;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import com.epam.infohandling.model.Lexeme;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends Parser {
    private static final String WORD_SPLITTER = "\\s";
    private static final String NUMBER_PATTERN = "\\d+";
    private static final String SIGN_PATTERN = "[+\\-*/]";
    private static final String EXPRESSION_PATTERN = NUMBER_PATTERN + WORD_SPLITTER +
            "((" + NUMBER_PATTERN  + "|" + SIGN_PATTERN + ")" + WORD_SPLITTER + ")+" + SIGN_PATTERN;
    private final static Pattern expressionPattern = Pattern.compile(EXPRESSION_PATTERN);
    private final static String EXPRESSION_SUBSTITUTE = "^expression^";

    public Component parse(final String text) {
        Queue<String> expressions = findExpressions(text);
        String substitutedExpressions = text.replaceAll(EXPRESSION_PATTERN, EXPRESSION_SUBSTITUTE);
        String[] parts = substitutedExpressions.split(WORD_SPLITTER);
        Component component = new Composite();
        for (String part : parts) {
            Component lexeme;
            if(part.equals(EXPRESSION_SUBSTITUTE)) {
                String expression = expressions.poll();
                lexeme = Lexeme.expression(expression);
            } else {
                lexeme = Lexeme.word(part);
            }
            component.addComponent(lexeme);
        }
        return component;
    }

    private Queue<String> findExpressions(final String text) {
        Queue<String> expressions = new ArrayDeque<>();
        Matcher expressionMatcher = expressionPattern.matcher(text);
        while(expressionMatcher.find()) {
            String expression = expressionMatcher.group();
            expressions.add(expression);
        }
        return expressions;
    }
}
