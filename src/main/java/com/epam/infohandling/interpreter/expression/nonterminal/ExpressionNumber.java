package com.epam.infohandling.interpreter.expression.nonterminal;

import com.epam.infohandling.interpreter.Context;
import com.epam.infohandling.interpreter.expression.MathExpression;

public class ExpressionNumber implements MathExpression {
    private int number;

    public ExpressionNumber(int number) {
        this.number = number;
    }

    @Override
    public void interpret(final Context context) {
        context.pushValue(number);
    }

}
