package com.epam.infohandling.interpreter.expression.terminal;

import com.epam.infohandling.interpreter.Context;
import com.epam.infohandling.interpreter.expression.MathExpression;

public class ExpressionPlus implements MathExpression {
    @Override
    public void interpret(final Context context) {
        int firstNumber = context.popValue();
        int secondNumber = context.popValue();
        context.pushValue(firstNumber + secondNumber);
    }
}
