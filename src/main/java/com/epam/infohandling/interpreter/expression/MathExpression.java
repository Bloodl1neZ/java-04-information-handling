package com.epam.infohandling.interpreter.expression;

import com.epam.infohandling.interpreter.Context;

public interface MathExpression {
    void interpret(Context context);
}
