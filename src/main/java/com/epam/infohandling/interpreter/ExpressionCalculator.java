package com.epam.infohandling.interpreter;

import com.epam.infohandling.interpreter.expression.MathExpression;
import com.epam.infohandling.interpreter.expression.nonterminal.ExpressionNumber;
import com.epam.infohandling.interpreter.expression.terminal.ExpressionDivide;
import com.epam.infohandling.interpreter.expression.terminal.ExpressionMinus;
import com.epam.infohandling.interpreter.expression.terminal.ExpressionMultiply;
import com.epam.infohandling.interpreter.expression.terminal.ExpressionPlus;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ExpressionCalculator {
    public int calculate(final String expression) {
        Context context = new Context();
        List<MathExpression> listExpression = parse(expression);
        for(MathExpression terminal : listExpression) {
            terminal.interpret(context);
        }
        return context.popValue();
    }

    private List<MathExpression> parse(final String expression) {
        List<MathExpression> listExpression = new ArrayList<>();
        String[] parts = expression.split("\\p{Blank}+");
        for(String part : parts) {
            if(part.isEmpty()) {
                continue;
            }
            char temp = part.charAt(0);
            switch (temp) {
                case '+':
                    listExpression.add(new ExpressionPlus());
                    break;
                case '-':
                    listExpression.add(new ExpressionMinus());
                    break;
                case '*':
                    listExpression.add(new ExpressionMultiply());
                    break;
                case '/':
                    listExpression.add(new ExpressionDivide());
                    break;
                    default:
                        Scanner scanner = new Scanner(part);
                        if (scanner.hasNextInt()) {
                            int nextInt = scanner.nextInt();
                            listExpression.add(new ExpressionNumber(nextInt));
                        }
                        break;
            }
        }
        return listExpression;
    }
}
