package com.epam.infohandling.interpreter;

import java.util.ArrayDeque;

public class Context {
    private ArrayDeque<Integer> contextValues = new ArrayDeque<>();

    public Integer popValue() {
        return contextValues.pop();
    }

    public void pushValue(final Integer value) {
        contextValues.push(value);
    }
}
