package com.epam.infohandling.decomposer;

import com.epam.infohandling.interpreter.ExpressionCalculator;
import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Lexeme;

import java.util.List;

public class TextDecomposer {
    private static final String PARAGRAPH_SPLITTER = "\n";
    private static final String WORD_SPLITTER = " ";

    private final ExpressionCalculator calculator = new ExpressionCalculator();

    public TextDecomposer() {

    }

    public String createText(final Component textComponent) {
        StringBuilder text = new StringBuilder();
        List<Component> paragraphs = textComponent.getChildren();
        paragraphs.forEach( paragraph -> addParagraphToString(paragraph, text) );
        return text.toString();
    }

    private void addParagraphToString(final Component paragraph, final StringBuilder text) {
        List<Component> sentences = paragraph.getChildren();
        sentences.forEach( sentence -> addWordsToString(sentence, text) );
        text.append(PARAGRAPH_SPLITTER);
    }

    private void addWordsToString(final Component sentence, final StringBuilder text) {
        List<Component> lexemes = sentence.getChildren();
        for(Component word : lexemes) {
            Lexeme lexeme = (Lexeme)word;
            String value = lexeme.getValue();
            if (lexeme.isExpression()) {
                Integer result = calculator.calculate(value);
                text.append(result.toString());
            } else {
                text.append(value);
            }
            text.append(WORD_SPLITTER);
        }
    }
}
