package com.epam.infohandling.cloner;

import com.epam.infohandling.model.Component;
import com.epam.infohandling.model.Composite;
import com.epam.infohandling.model.Lexeme;
import com.epam.infohandling.model.Value;

import java.util.List;

public class TextCloner {
    public Component clone(final Component text) {
        List<Component> paragraphs = text.getChildren();
        Component clonedText = new Composite();
        for (Component paragraph : paragraphs) {
            Component clonedParagraph = cloneParagraph(paragraph);
            clonedText.addComponent(clonedParagraph);
        }
        return clonedText;
    }

    private Component cloneParagraph(final Component paragraph) {
        List<Component> sentences = paragraph.getChildren();
        Component clonedParagraph = new Composite();
        for (Component sentence : sentences) {
            Component clonedSentence = cloneSentence(sentence);
            clonedParagraph.addComponent(clonedSentence);
        }
        return clonedParagraph;
    }

    private Component cloneSentence(final Component sentence) {
        List<Component> lexemes = sentence.getChildren();
        Component clonedSentence = new Composite();
        for (Component lexeme : lexemes) {
            String value = ((Value)lexeme).getValue();
            boolean expression = ((Lexeme)lexeme).isExpression();
            Component clonedLexeme = (expression) ? Lexeme.expression(value) : Lexeme.word(value);
            clonedSentence.addComponent(clonedLexeme);
        }
        return clonedSentence;
    }
}
