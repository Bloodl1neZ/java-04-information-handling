package com.epam.infohandling.model;

import java.util.ArrayList;
import java.util.List;

public class Composite implements Component {
    private List<Component> components = new ArrayList<>();

    public void addComponent(final Component component) {
        components.add(component);
    }

    public List<Component> getChildren() {
        return components;
    }
}
