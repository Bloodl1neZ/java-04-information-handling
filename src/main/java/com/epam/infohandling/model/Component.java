package com.epam.infohandling.model;
import java.util.List;
public interface Component {
    void addComponent(Component component);
    List<Component> getChildren();
}
