package com.epam.infohandling.model;

import java.util.List;

public class Lexeme implements Component, Value {
    private final String value;
    private boolean expression;


    private Lexeme(final String value, boolean expression) {
        this.value = value;
        this.expression = expression;
    }

    public static Lexeme word(final String value) {
        return new Lexeme(value, false);
    }

    public static Lexeme expression(final String value) {
        return new Lexeme(value, true);
    }

    public void addComponent(final Component component) {
        throw new UnsupportedOperationException();
    }

    public List<Component> getChildren() {
        throw new UnsupportedOperationException();
    }

    public String getValue() {
        return value;
    }

    public boolean isExpression() {
        return expression;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lexeme lexeme = (Lexeme) o;

        if (expression != lexeme.expression) return false;
        return value != null ? value.equals(lexeme.value) : lexeme.value == null;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (expression ? 1 : 0);
        return result;
    }
}
