package com.epam.infohandling.model;

public interface Value {
    String getValue();
}
